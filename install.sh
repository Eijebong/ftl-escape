#!/bin/bash

# Tested on Debian 7
# By default, setup ftl-escape in /var/www/
# by Oros

# Path where we setup ftl-escape
www="/var/www/"

if [ "$EUID" -ne 0 ]; then 
	echo -e "\033[31mPlease run as root\033[0m" 1>&2
	exit 1
fi
read -p "/!\ This script is highly experimental. You should review it before executing it. Do you really want to continue ? (y/N)" -n 1 -r
echo 
if [[ $REPLY =~ ^[Yy]$ ]]
then
	if [ ! "`which apache2`" ]; then
		apt-get install -y apache2 php5
		echo "<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	DocumentRoot ${www}
	<Directory />
		Options FollowSymLinks
		AllowOverride None
	</Directory>
	<Directory ${www}>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Order allow,deny
		allow from all
	</Directory>
	ErrorLog \${APACHE_LOG_DIR}/error.log
	LogLevel warn
	CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>" > /etc/apache2/sites-available/default
		rm ${www}index.html
		newapache=1
	else
		newapache=0
	fi

	apt-get install -y unzip php5 php5-sqlite
	mkdir -p $www
	cd $www
	wget -q https://getcomposer.org/installer -O- | php
	wget -q https://git.framasoft.org/llm/ftl-escape/repository/archive.zip
	unzip -q archive.zip
	rm archive.zip
	mv ftl-escape.git/* .
	rm -r ftl-escape.git
	a2enmod rewrite
	echo 'Options -Indexes
<Files "db.sqlite">
	Deny from all
</Files>
RewriteEngine On
RewriteBase /
RewriteRule ^(cron|admin|dics|lang|lib|proxies|src|tpl)(/.*|)$ - [NC,F]
' > .htaccess
	php composer.phar install
	vendor/bin/doctrine orm:schema-tool:create
	vendor/bin/doctrine orm:generate-proxies
	chown -R www-data:www-data $www
	chmod -R 775 $www
	if [ ! "`grep -r smarty3 /etc/php5/apache2/*`" ]; then
		echo '[smarty3]
include_path = ".:/usr/share/php:/usr/share/php/smarty3"' > /etc/php5/apache2/conf.d/smarty.ini
	fi
	apache2ctl restart

	if [ newapache ]; then
		wget -q http://127.0.0.1/filldb.php -O /dev/null
	else
		echo -e "\033[31mPlease check in your apache conf (/etc/apache2/sites-available/default) that \"AllowOverride All\" is present in \"<Directory ${www}>\".\033[0m"
		echo "You can access to http://yourserver/filldb.php"
	fi

	echo "59 * * * * www-data php ${www}cron/encounter.php
0 * * * * www-data php ${www}cron/food.php
0 * * * * www-data php ${www}cron/fuel.php
0 * * * * www-data php ${www}cron/medicine.php
*/5 * * * * www-data php ${www}cron/mining.php
0 * * * * www-data php ${www}cron/moral.php
1 * * * * www-data php ${www}cron/populatesectors.php
0 */6 * * * www-data php ${www}cron/qualifiedstaff.php
0 0 * * * www-data php ${www}cron/birth.php" > /etc/cron.d/ftl-escape

	echo "End of setup "
	echo ":-)"
fi