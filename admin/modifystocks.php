<?php

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$player = $entityManager->getRepository('Player')->find($_POST['id']);
$i18n = new I18n();
$i18n->autoSetLang();

if (is_numeric($_POST['food']) && is_numeric($_POST['fuel']) && is_numeric($_POST['material']) && is_numeric($_POST['moral']) && is_numeric($_POST['medicine']) && is_numeric($_POST['staff']))
{
    $fleet = $player->getFleet();
    $fleet->setFood($_POST['food']);
    $fleet->setFuel($_POST['fuel']);
    $fleet->setMaterial($_POST['material']);
    $fleet->setMoral($_POST['moral']);
    $fleet->setMedicine($_POST['medicine']);
    $fleet->setQualifiedStaff($_POST['staff']);
    $message = new Message(null,$player,'msg.admin.stocks.modified',true);
    $entityManager->persist($message);
}

$entityManager->flush();
header('Location: index.php');