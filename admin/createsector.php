<?php

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$player = $entityManager->getRepository('Player')->find($_GET['playerid']);
$i18n = new I18n();
$i18n->autoSetLang();

$sector = Builder::buildSector($player);

$entityManager->persist($sector);
$entityManager->flush();
header('Location: index.php');