<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');
require_once($docroot.'/builder.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
    $objective = $player->getObjectiveType();

	$sector = $player->getSector();

	$i18n = new I18n();
	$i18n->autoSetLang();

	if (!is_null($sector) && Helper::canAct($player))
	{
		if ($sector->getSearched() < 100)
		{
			if ($objective == OBJECTIVE_SEARCH_HABITABLE_ID)
			{
                if (!$sector->isHabitable())
                {
                    $fleet = $player->getFleet();
                    if (Helper::canScout($fleet))
                    {
                        $ships = $fleet->getShips();
                        $nbscout = 0;
                        foreach ($ships as $ship)
                        {
                            if ($ship->getType()->canScout())
                            {
                                $nbscout++;
                                $dice = rand(1,10000);
                                if ($dice <= CHANCE_TO_FIND_HABITABLE)
                                {
                                    $sector->setHabitable();
                                }
                            }
                        }
                        if ($sector->isHabitable())
                        {
                            $message = new Message(null,$player,'msg.found.habitable.planet',true);
                            $entityManager->persist($message);
                        }
                        $sector->increaseSearch($nbscout);
                    }
                }
			}
			elseif ($objective == OBJECTIVE_SEARCH_EARTH_ID)
			{
                if ($player->getEarthClues() < NB_CLUES_TO_EARTH)
                {
                    $fleet = $player->getFleet();
                    if (Helper::canScout($fleet))
                    {
                        $ships = $fleet->getShips();
                        $nbscout = 0;
                        foreach ($ships as $ship)
                        {
                            if ($ship->getType()->canScout())
                            {
                                $nbscout++;
                                $dice = rand(1,10000);
                                if ($dice <= CHANCE_TO_FIND_EARTH_CLUE)
                                {
                                    $message = new Message(null,$player,'msg.found.earth.clue',true);
                                    $entityManager->persist($message);
                                    $player->findEarthClue();
                                }
                            }
                        }
                        $sector->increaseSearch($nbscout);
                    }
                }
			}
		}
	}
}
$entityManager->flush();