<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');
require_once($docroot.'/builder.php');
require_once($docroot.'/helper.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	if (Helper::canAct($player))
	{
		$dice = rand(1,100);
		if ($dice <= CHANCE_OF_EVENT)
		{
			$events = $entityManager->getRepository('EventType')->findAll();
			$nbEvents = count($events);
			$eventId = rand(1,$nbEvents);
			$event = $entityManager->getRepository('EventType')->find($eventId);
			if ($event->matchConditions($player))
			{
				$player->setEvent($event);
			}
		}
	}
}

$entityManager->flush();
