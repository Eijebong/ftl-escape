<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$qb = $entityManager->createQueryBuilder();

$qb->select('p')
	->from('Player','p')
	->where('p.deletiondate < ?1')
	->setParameter(1,time()-DELETION_PERIOD);
	
$players = $qb->getQuery()->getResult();

foreach ($players as $player)
{
	$entityManager->remove($player);
}
$entityManager->flush();
