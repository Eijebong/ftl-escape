<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$fleets = $entityManager->getRepository('Fleet')->findAll();

foreach ($fleets as $fleet)
{
    $politicalsystem = $fleet->getPoliticalSystem();
    if (!is_null($politicalsystem) && $politicalsystem->getElection())
    {
        $qb = $entityManager->createQueryBuilder();
        
        $qb->select('c')
            ->from('Character','c')
            ->where('c.fleet = ?1')
            ->setParameter(1,$fleet)
            ->orderBy('c.popularity','DESC');
            
        $characters = $qb->getQuery()->getResult();
        $cpt = 1;
        $max=100;
        $elected = null;
        $scoreElected = 0;
        foreach ($characters as $character)
        {
            $popularity = $character->getPopularity();
            $result=0;
            switch($cpt)
            {
                case 1: $result = rand(ELECTIONS_FIRST_MIN_PERCENT,$max);$max -= $result;break;
                case 2: $min = ELECTIONS_SECOND_MIN_PERCENT; if (ELECTIONS_SECOND_MIN_PERCENT > $max) {$min = 0;} $result = rand($min,$max); $max -= $result;break;
                case 3: $min = ELECTIONS_THIRD_MIN_PERCENT; if (ELECTIONS_THIRD_MIN_PERCENT > $max) {$min = 0;} $result = rand($min,$max); $max -= $result;break;
                default: $result = rand(0,$max); $max -= $result;
            }
            if ($result > $scoreElected)
            {
                $elected = $character;
                $scoreElected = $result;
            }
            $cpt++;
        }
        if (!is_null($fleet->getFakedElection()))
        {
            $popControl = $politicalsystem->getPopulationControl();
            $dice = rand(1,100);
            if ($dice > $popControl)
            {
                $fakedPeriod = time() - $fleet->getFakedElectionTime();
                $percent = round($fakedPeriod / ELECTIONS_MAX_TIME * 100);
                $dice = rand(1,100);
                if ($dice > $percent)
                {
                    $fleet->decreaseMoral(ELECTIONS_FAKED_MORAL_MALUS);
                    $killed = rand(1,ELECTIONS_FAKE_MAX_KILLS);
                    Helper::killRandomPopulation($fleet,$killed);
                    $message = new Message(null,$fleet->getPlayer(),'msg.election.troubles',true,array($killed,ELECTIONS_FAKED_MORAL_MALUS));
                    $entityManager->persist($message);
                }
            }
            $elected = $fleet->getFakedElection();
        }
        $message = new Message(null,$fleet->getPlayer(),'msg.character.elected',true,array($elected->getName(),$scoreElected));
		$entityManager->persist($message);
        $fleet->setChief($elected);
        if ($elected->getPoliticalSystem()->getId() != $politicalsystem->getId())
        {
            $fleet->setPoliticalSystem($elected->getPoliticalSystem());
            $message = new Message(null,$fleet->getPlayer(),'msg.political.system.changed',true,array($elected->getName()));
            $entityManager->persist($message);
        }
        $fleet->setFakedElection(null);
    }
}
$entityManager->flush();