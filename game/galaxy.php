<?php

$smarty->assign('players',$entityManager->getRepository('Player')->findAll());
$smarty->assign('planets',$entityManager->getRepository('Planet')->findAll());

// i18n
$smarty->assign('i18n',$i18n);
$smarty->assign('lbl_player',$i18n->getText('lbl.commander'));
$smarty->assign('lbl_nb_ships',$i18n->getText('lbl.nb.ships'));
$smarty->assign('lbl_nb_survivors',$i18n->getText('lbl.nb.survivors'));
$smarty->assign('lbl_galaxy',$i18n->getText('lbl.menu.galaxy'));
$smarty->assign('lbl_freed_by',$i18n->getText('lbl.freed.by'));
$smarty->assign('lbl_attacked_by',$i18n->getText('lbl.attacked.by'));
$smarty->assign('lbl_planets',$i18n->getText('lbl.planets'));
