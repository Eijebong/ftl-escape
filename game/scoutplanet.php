<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_GET['token']))
{
    if (is_numeric($_GET['id']))
    {
        if (Helper::canScout($player->getFleet()))
        {
            $planet = $entityManager->find('Planet',$_GET['id']);
            if (!$planet->isAttacked() && $planet->getStatus() == PLANET_STATUS_OCCUPIED)
            {
                $planet->spy();
            }
        }
    }
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header('Location:index.php?page=politics');

$entityManager->flush();