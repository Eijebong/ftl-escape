<?php

print_r($_POST);
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$i18n = new I18n();
$i18n->autoSetLang();
if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);
    
    if (Helper::canAct($player))
    {
        if (is_numeric($_POST['characterid']) && is_numeric($_POST['ship']))
        {
            $character = $entityManager->getRepository('Character')->find($_POST['characterid']);
            if ($character->getFleet()->getId() == $player->getFleet()->getId())
            {
                $ship = $entityManager->getRepository('Ship')->find($_POST['ship']);
                if ($ship->getFleet()->getId() == $player->getFleet()->getId())
                {
                    $character->setShip($ship);
                }
            }
        }
    }
	$entityManager->flush();
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header('Location: index.php?page=characters');