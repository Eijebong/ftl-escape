<?php

print_r($_POST);
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once __DIR__.'/../bootstrap.php';
require_once __DIR__.'/../const.php';
require_once __DIR__.'/../builder.php';
require_once __DIR__.'/../helper.php';

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);
	if (array_key_exists('answer',$_POST))
	{
		if(is_numeric($_POST['answer']))
		{
			$answer = $_POST['answer'];
			$event = $player->getEvent();
			if (is_null($event))
			{
				Tools::setFlashMsg($i18n->getText('msg.no.event'));
			}
			else
			{
				if ($event->resolveEvent($answer,$player))
				{
					$player->setEvent(null);
				}
				else
				{
					Tools::setFlashMsg($i18n->getText('msg.impossible.to.solve.event'));
				}
				$entityManager->flush();
			}
		}
		else
		{
			echo "Nope.";
			exit;
		}
	}
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header('Location: index.php');
