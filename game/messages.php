<?php

$qb = $entityManager->createQueryBuilder();

$qb->select('m')
	->from ('Message', 'm')
	->where ('m.recipient = :player and m.recipientdeleted = false and m.sender is null')
	->orderBy('m.time','desc')
	->setParameter('player',$player);

$query = $qb->getQuery();

$notifications = $query->getResult();

foreach ($notifications as $notification)
{
	$notification->read();
}
$entityManager->flush();
$qb = $entityManager->createQueryBuilder();

$qb->select('m')
	->from ('Message', 'm')
	->where ('m.recipient = :player and m.recipientdeleted = false and m.sender is not null')
	->orderBy('m.time','desc')
	->setParameter('player',$player);

$query = $qb->getQuery();

$messages = $query->getResult();

$smarty->assign('i18n',$i18n);
$smarty->assign('notifications',$notifications);
$smarty->assign('messages',$messages);
$smarty->assign('length',TRUNCATED_MESSAGE_LENGTH);
$smarty->assign('lbl_received_messages',$i18n->getText('lbl.received_messages'));
$smarty->assign('messages_th_message',$i18n->getText('messages.th.message'));
$smarty->assign('messages_th_date',$i18n->getText('messages.th.date'));
$smarty->assign('messages_th_sender',$i18n->getText('messages.th.sender'));
$smarty->assign('messages_no_notif',$i18n->getText('messages.no_notif'));
$smarty->assign('messages_no_message',$i18n->getText('messages.no_message'));
$smarty->assign('messages_send',$i18n->getText('messages.send'));
$smarty->assign('messages_recipient',$i18n->getText('messages.recipient'));

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
