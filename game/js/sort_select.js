function sortSelect(e){
	if (e != null)
	{
		var oA,i,o;oA=[];
		for(i=0;i<e.options.length;i++)
		{
			o=e.options[i];
			oA[i]=new Option(o.text,o.value,o.defaultSelected,o.selected);
		}
		oA.sort(function(a,b){
			var la=a.text.toLowerCase(),lb=b.text.toLowerCase();if(la>lb){return 1;}
			if(la<lb )
			{
				return-1;
			}
			return 0;
		});
		e.options.length=0;
		for(i=0;i<oA.length;i++)
		{
			e.options[i]=oA[i];oA[i]=null;
		}
		return true;
	}
}
