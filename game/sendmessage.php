<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	print_r($_POST);

	$message = htmlentities($_POST['message']);
	$recipient = $entityManager->getRepository('Player')->findOneByLogin($_POST['recipient']);

	if (is_null($recipient))
	{
		Tools::setFlashMsg($i18n->getText('msg.message.recipient.not.found'));
	}
	else
	{
		$messageItem = new Message($player,$recipient,$message);
		Tools::setFlashMsg($i18n->getText('msg.msg.sent'));
		$entityManager->persist($messageItem);
		$entityManager->flush();
	}
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header ('Location: index.php?page=messages');
