<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_GET['token']))
{
    $fleet = $player->getFleet();
    $material = $fleet->getMaterial();
    $uranium = $fleet->getUranium();
    if ($uranium >= NUCLEAR_URANIUM_NEEDED && $material >= NUCLEAR_MATERIAL_NEEDED && $fleet->getNuclearWeapons() < $fleet->getMaxNuclearWeapons())
    {
        $fleet->setNuclearWeapons($fleet->getNuclearWeapons() + 1);
        $fleet->setUranium($uranium - NUCLEAR_URANIUM_NEEDED);
        $fleet->setMaterial($material - NUCLEAR_MATERIAL_NEEDED);
        $entityManager->flush();
    }
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header('Location: index.php?page=fleet');
