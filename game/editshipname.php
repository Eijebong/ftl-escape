<?php

$shipid = $_GET['id'];
$problem = !is_numeric($shipid);

if (!$problem)
{
	$fleet = $player->getFleet();
	$ship = $fleet->getShip($shipid);
	$problem = is_null($ship);
	if (!$problem)
	{
		$smarty->assign('shipid',$shipid);
		$smarty->assign('shipname',$ship->getName());
	}
}

$smarty->assign('problem',$problem);

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
