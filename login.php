<?php
require_once __DIR__."/bootstrap.php";
require_once __DIR__."/tools.php";
include __DIR__.'/lib/session.inc.php';
if (isset($_POST['login']) && isset($_POST['password']))
{
	if (Tools::login_exists($_POST['login']) && check_auth($_POST['login'], $_POST['password']))
	{
		header('Location: game/index.php');
	}
	else
	{
		header('Location: index.php?errorno=1');
	}
}
else
{
	header('Location: index.php?errorno=3');
}
