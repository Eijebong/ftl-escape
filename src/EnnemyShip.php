<?php
/**
 * @Entity @Table(name="ennemyships")
 **/
class EnnemyShip
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @ManyToOne(targetEntity="EnnemyShipType") **/
    private $type;
    /** @Column(type="integer") **/
    protected $hp;
    /** @ManyToOne(targetEntity="Sector", inversedBy="ennemies")
     * @var Sector
     */
    private $sector;
    /** @Column(type="string") **/
    private $name="unknown";
    
    public function __construct($sector,$type,$name)
    {
		$this->sector=$sector;
		$this->name=$name;
		$this->setType($type);
		$sector->addEnnemy($this);
	}
    
    public function getId()
    {
		return $this->id;
	}
	
	public function setHP($hp)
	{
		$this->hp=$hp;
	}
	
	public function getHP()
	{
		return $this->hp;
	}
	
	public function setType($type)
	{
		$this->type = $type;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function setSector($sector)
	{
		$this->sector=$sector;
	}
	
	public function getSector()
	{
		return $this->sector;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function takeDamage($damage)
	{
		$damage -= $this->type->getDefense();
		if ($damage > 0)
		{
			$this->hp -= $damage;
		}
	}
}
